#include <hash.h>
#include <shards.h>
#include <hash.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <persistence.h>

shardlist_t* shard_init(int size) {
	shardlist_t *shardlist;
	int i;
	shardlist = (shardlist_t*) malloc(sizeof(shardlist_t));

	shardlist->list = malloc( sizeof(shard_t) * size );

	shardlist->size = size;
	for(i = 0; i < size; i++) {
		shardlist->list[i] = (shard_t*)malloc(sizeof(shard_t));
		//Relacionado com a escrita em disco
		shardlist->list[i]->NPKV = 0;
		//Nota: O persistence init inicializa muitas das estruturas relacionadas com persistencia

		shardlist->list[i]->id = i;
		shardlist->list[i]->hashtable = hash_init(HT_SIZE);
		pthread_mutex_init(&shardlist->list[i]->locker, NULL);
		pthread_mutex_init(&shardlist->list[i]->persistence_lock, NULL);
		//inicialização da persistencia em disco
		persistence_init(shardlist->list[i]); 
	}

	return shardlist;

}

shard_t* shard_get(shardlist_t *shardlist, int id) {
	if(shardlist->size < id)
		return NULL;
	return shardlist->list[id];
}

char* shard_insert_key(shard_t* shard, char key[KV_SIZE], char value[KV_SIZE]) {
	KV_t temp_kv;
	//Inicializamos a estrutura temporária
	strncpy(temp_kv.key, key, KV_SIZE);
	strncpy(temp_kv.value, value, KV_SIZE);

	pthread_mutex_lock(&shard->locker);
		hashtable_t* hash = shard->hashtable;
		char* oldvalue = hash_set(hash, key, value);
		//Actualizamos a kv em disco
		update_kv_pair(shard, temp_kv);
	pthread_mutex_unlock(&shard->locker);

	return oldvalue;
}

char* shard_get_key(shard_t* shard, char key[KV_SIZE]) {
	pthread_mutex_lock(&shard->locker);
	hashtable_t* hash = shard->hashtable;
	char* value = hash_get(hash, key);
	pthread_mutex_unlock(&shard->locker);

	return value;

}