#include <buffer.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

buffer_t* buffer_init(int size, int num_servers) {
	buffer_t *buffer;

	//Inicialização da estrutura em memória
	buffer = (buffer_t*) malloc(sizeof(buffer_t));
	//Inicialização de variaveis simples
	buffer->size = size;
	//Inicilizar os trincos
	pthread_mutex_init(&buffer->lock, NULL);
	//Inicialização dos semaforos dos prod-cons
	sem_init(&buffer->posicoesComInfo,0,0);
	sem_init(&buffer->posicoesSemInfo,0,size);
	//Inicialização dos indexers
	buffer->proximaEscrita = 0;
	buffer->proximaLeitura = 0;
	//Inicilialização das estruturas de resposta
	buffer->buff = malloc( sizeof(response_t) * size);

	return buffer;
}

response_t* buffer_new_response(int shardid, int command, char* key, char* value) {
	response_t* response;
	response = (response_t*) malloc(sizeof(response_t));

	response->shardid = shardid;
	response->command = command;
	if(key!=NULL)
		strncpy(response->key, key, KV_SIZE);
	if(value!=NULL)
		strncpy(response->value, value, KV_SIZE);	
	response->status = UNPROCESSED;
	response->integer = -1;
	response->response = NULL;
	pthread_mutex_init(&response->lock, NULL);
	sem_init(&response->valid_response, 0, 0);

	return response;
}
/**
 * Equivale ao depositaItem dos prod-cons
 */
void buffer_insert_request(buffer_t* buffer, response_t* request) {
	int indice;

	sem_wait(&buffer->posicoesSemInfo); //Esperamos que hajam posições livres no buffer
	pthread_mutex_lock(&buffer->lock);
	indice = (buffer->proximaEscrita++) % buffer->size;
	buffer->buff[indice] = request;
	pthread_mutex_unlock(&buffer->lock);
	sem_post(&buffer->posicoesComInfo); //Informamos os interessados que há mais um pedido

}

response_t* buffer_pop_request(buffer_t* buffer) {
	int indice;
	response_t* response = NULL;
	sem_wait(&buffer->posicoesComInfo);
	pthread_mutex_lock(&buffer->lock);
	indice = (buffer->proximaLeitura++) % buffer->size;
	response = buffer->buff[indice];
	buffer->buff[indice] = NULL; //Retiramos o obj response do buffer
	pthread_mutex_unlock(&buffer->lock);
	sem_post(&buffer->posicoesSemInfo);
	return response;
}