#include <kos_client.h>
#include <buffer.h>
#include <stdlib.h>
#include <stdio.h>
#include <hash.h>
#include <shards.h>
#include <string.h>
#include <server.h>
#include <pthread.h>
#include <semaphore.h>
#include <persistence.h>

buffer_t* buffer;
pthread_t* threads;
shardlist_t* shardlist;

int kos_init(int num_server_threads, int buf_size, int num_shards) {

	int i, code;
	
	//Inicializar o buffer
	buffer = buffer_init(buf_size, num_server_threads);

	//Inicializar as shards
	shardlist = shard_init(num_shards);

	//Inicializar o servidor
	int* ids=(int*) malloc(sizeof(int)*num_server_threads);
	threads = (pthread_t*)malloc(sizeof(pthread_t)*num_server_threads);	
	for(i=0;i<num_server_threads;i++) {
		ids[i] = i;
		if ( (code=pthread_create(&threads[i], NULL, &server_thread, &(ids[i]) ) ) ) {
			printf("pthread_create failed with code %d!\n",code);
			return -1;
		}
	}
	return 0;

}




char* kos_get(int clientid, int shardId, char* key) {
	char* text;
	//Vou buscar o meu buffer respectivo.
	response_t* request = buffer_new_response(shardId, GET, key, NULL);
	buffer_insert_request(buffer, request); //Bloqueia-se a espera de um servidor
	//notificamos agora o servidor que colocamos lá treta, e ficamos à espera de uma resposta
	sem_wait(&request->valid_response);
	text = (char*)request->response;

	free(request);
	return text;
}




char* kos_put(int clientid, int shardId, char* key, char* value) {
	char* text;
	//Vou buscar o meu buffer respectivo.
	response_t* request = buffer_new_response(shardId, PUT, key, value);
	buffer_insert_request(buffer, request); //Bloqueia-se a espera de um servidor
	//Ficamos a espera que a resposta seja válida
	sem_wait(&request->valid_response);
	text = (char*)request->response;

	//if(text==NULL)
	//	printf("Ooooooppss!!! Vou rebentar a put daqui a nada...\n");

	free(request);

	return text;
}

char* kos_remove(int clientid, int shardId, char* key) {
	char* text;
	//Vou buscar o meu buffer respectivo.
	response_t* request = buffer_new_response(shardId, REMOVE, key, NULL);

	buffer_insert_request(buffer, request); //Bloqueia-se a espera de um servidor
	sem_wait(&request->valid_response);
	text = (char*)request->response;

	free(request);
	return text;
}

KV_t* kos_getAllKeys(int clientid, int shardId, int* dim) {

	KV_t *response;
	//Vou buscar o meu buffer respectivo.
	response_t* request = buffer_new_response(shardId, GETALLKEYS, NULL, NULL);
	
	buffer_insert_request(buffer, request); //Bloqueia-se a espera de um servidor
	sem_wait(&request->valid_response);
	*dim = request->integer;
	response = (KV_t*)request->response;
	
	free(request);
	return response;
	
}


