#include <server.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <delay.h>
#include <persistence.h>
void *server_thread(void *arg) {

	//int server_id=*( (int*)arg);
	char* response_value = "ad";
	//bpair_t* pair;
	response_t* response;

	while(1) {
		//Inicar um trinco
		response = buffer_pop_request(buffer); //Bloqueia-se se não houver nada a processar.

		int command = response->command;
		delay();
		switch(command) {
			case GET:
				response_value = server_get(shardlist, response->shardid, response->key);
				response->response = (char*)response_value;
				break;
			case PUT:
				response_value = server_put(shardlist, response->shardid, response->key, response->value);
				response->response = (char*)response_value;
				break;

			case REMOVE:
				response_value = server_remove(shardlist, response->shardid, response->key);
				response->response = (char*)response_value;
				break;

			case GETALLKEYS:
				response->response = (KV_t*)server_get_all_keys(shardlist, response->shardid, &response->integer);
				break;

			default:
				printf("Pedido default?");
				break;
		}

		sem_post(&response->valid_response);
		//Já processei, zimbora lá avisar o cliente

	}

	return NULL;

}

char* server_get(shardlist_t* shardlist, int shardid, char key[KV_SIZE]) {
	char* return_text;

	shard_t* shard = shard_get(shardlist, shardid);
	pthread_mutex_lock(&shard->locker);
	hashtable_t* hash = shard->hashtable;
	return_text = hash_get(hash, key);
	pthread_mutex_unlock(&shard->locker);

	return return_text;
}

char* server_put(shardlist_t* shardlist, int shardid, char key[KV_SIZE], char value[KV_SIZE]) {
	char* return_text;
	
	shard_t* shard = shard_get(shardlist, shardid);
	return_text = shard_insert_key(shard, key, value); //Já se bloqueia 

	return return_text;

}

char* server_remove(shardlist_t* shardlist, int shardid, char key[KV_SIZE]) {
	char* return_text;
	//int i;
	shard_t* shard = shard_get(shardlist, shardid);
	pthread_mutex_lock(&shard->locker);
	hashtable_t* hash = shard->hashtable;
	return_text = hash_remove(hash, key);

	remove_kv_pair(shard,key);
	pthread_mutex_unlock(&shard->locker);
	return return_text;
}

KV_t* server_get_all_keys(shardlist_t* shardlist, int shardid, int* dim) {
	if(shardid >= shardlist->size) {
		*dim = -1;
		return NULL;
	}

	shard_t* shard = shard_get(shardlist, shardid);
	pthread_mutex_lock(&shard->locker);
	hashtable_t* hash = shard->hashtable;
	int i;
	int counter=0;
	hashpair_t* pair = NULL;

	KV_t* vector = (KV_t*)malloc(sizeof(KV_t));

	for(i=0; i < hash->size; i++) {
		pair = hash->table[i];
		if(pair==NULL) continue;

		while(pair!=NULL) {
			vector[counter++] = *pair->kv;
			vector = (KV_t*)realloc(vector, (counter+1) * sizeof(KV_t));
			pair = pair->next;
		}

	}
	pthread_mutex_unlock(&shard->locker);

	*dim = counter;
	return vector;
}