#include <unistd.h>
#include <hash.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define HT_SIZE 10
 
int hash(char* key) {
     
    int i=0;
 
    if (key == NULL)
        return -1;
 
    while (*key != '\0') {
        i+=(int) *key;
        key++;
    }
 
    i=i % HT_SIZE;
 
    return i;
}

hashtable_t* hash_init(int size) {
	hashtable_t *hashtable = NULL;
	int i; //Counter

	if(size < 1) return NULL;

	/* Init the hashtable structure */
	hashtable = malloc( sizeof(hashtable_t) );
	if(hashtable==NULL) //O malloc falhou
		return NULL;

	/* init of the content*/
	hashtable->table = malloc( sizeof( hashpair_t* )*size );
	if(hashtable->table == NULL) //erro de malloc
		return NULL;

	for(i = 0; i < size; i++)
		hashtable->table[i] = NULL;
 	
	hashtable->size = size;

	return hashtable;

}

hashpair_t* hash_newpair( char key[KV_SIZE], char value[KV_SIZE] ) {
	hashpair_t *pair=NULL;

	/* Create the pair structure */
	pair = malloc(sizeof(hashpair_t));
	if(pair==NULL) return NULL;

	/* Initialize the keyvalue structure */
	KV_t *keyvalue  = malloc(sizeof(KV_t));
	if(keyvalue==NULL) return NULL;

	strncpy(keyvalue->key,key,KV_SIZE);
	strncpy(keyvalue->value,value,KV_SIZE);
	
	pair->kv = keyvalue;
	pair->next = NULL;

	return pair;

}

char* hash_set( hashtable_t *hashtable, char key[KV_SIZE], char value[KV_SIZE] ) {
	int hashkey;
	hashkey = hash(key);
	hashpair_t *next = hashtable->table[hashkey];
	char* return_value;
	hashpair_t *newpair;
	hashpair_t *last;

	while( next != NULL && next->kv->key != NULL && strcmp( key, next->kv->key )>0 ) {
		last = next; //Store the last value
		next = next->next;
	}

	//If there is a pair, replace it
	if( next!= NULL && next->kv->key != NULL &&  strcmp( key, next->kv->key ) == 0) {
		//temos que copiar o valor para return_value, mas antes temos que iniciar a string.
		return_value = malloc(sizeof(char)*(KV_SIZE+1));
		strncpy(return_value,next->kv->value,KV_SIZE);
		strncpy(next->kv->value,value,KV_SIZE);
		return return_value;
	}

	//Okey, we've to create a new item on the hash
	newpair = hash_newpair(key, value);
	if( next == hashtable->table[hashkey] ) {
		//No inicio da hashtable
		newpair->next = next;
		hashtable->table[hashkey] = newpair;
 	} else if (next == NULL) {
 		//No final da hashtable
 		last->next = newpair;
 	} else {
 		//No meio da lista
 		newpair->next = next;
 		last->next = newpair;
 	}
 	return NULL;

}

char* hash_get( hashtable_t *hashtable, char key[KV_SIZE]) {
	int hashkey;
	hashkey = hash(key);

	hashpair_t *pair = hashtable->table[hashkey];

	while( pair != NULL && pair->kv->key != NULL && strcmp( key, pair->kv->key )>0 ) {
		pair = pair->next;
	}

	if( pair == NULL || pair->kv->key == NULL || strcmp( key, pair->kv->key ) != 0) {
		return NULL;
	}

	return pair->kv->value;

}

char* hash_remove( hashtable_t *hashtable, char key[KV_SIZE]) {
	int hashkey;
	hashkey = hash(key);
	hashpair_t *current = hashtable->table[hashkey];
	hashpair_t *previous;
	char* return_value;
	previous = current;
	while(current != NULL) {
		if( strcmp(key, current->kv->key)==0 ) {
			return_value = current->kv->value;

			if(previous == current) {
				//Primeiro elemento
				hashtable->table[hashkey] = current->next;
				free(current);
			} else if(current->next == NULL) {
				previous->next = NULL;
				free(current);
			} else {
				previous->next = current->next;
				free(current);
			}

			return return_value; //Saimos do ciclo porque já apagamos
		}

		previous = current;
		current = current->next;
	}
	return NULL;
 	
}