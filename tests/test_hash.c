#include <kos_client.h>
#include <stdio.h>
#include <shards.h>
#include <string.h>
#include <persistence.h>
#define NUM_EL 6
#define NUM_SHARDS 3

int NPKV=0;
KV_t* kvs[10];
KV_t kvs_copied[10];
FILE* file;



int main(int argc, char *argv[]) {


	char key[20], value[20];
	char* v;
	int i,j,ret;
	int client_id=1;

	ret=kos_init(1, 5, NUM_SHARDS);


	for (j=NUM_SHARDS-1; j>=0; j--) {	
		for (i=0; i<NUM_EL; i++) {
			sprintf(key, "k%d",i);
			v=kos_get(client_id,j, key);
			printf("Element %s %s found in shard %d: value=%s\n", key, ( v==NULL ? "has not been" : "has been" ),j,
									( v==NULL ? "<missing>" : v ) );
	
		}
	}


	printf("A tentar remover...\n");
	kos_remove(client_id,1,"k4");
	kos_remove(client_id,1,"k6");

	return 0;
}