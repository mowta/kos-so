#ifndef HASH_H
#define HASH_H 1
#include <kos_client.h>

#define HT_SIZE 10


typedef struct hashpair_t {
	KV_t *kv;
	struct hashpair_t *next;
} hashpair_t;

typedef struct hashtable_t {
	int size;
	hashpair_t **table; //the hash table;
} hashtable_t;


//the hashkey generator
int hash(char* key);

//initialize the hashtable
hashtable_t* hash_init(int size);

hashpair_t* hash_newpair(char key[KV_SIZE], char value[KV_SIZE]);

//insert in the hashtable
char* hash_set( hashtable_t *hashtable, char key[KV_SIZE], char value[KV_SIZE] );

//get a pair
char* hash_get( hashtable_t *hashtable, char key[KV_SIZE]);

char* hash_remove( hashtable_t *hashtable, char key[KV_SIZE]);

#endif