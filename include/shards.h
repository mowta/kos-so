#ifndef SHARD_H
#define SHARD_H 1

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <hash.h>
#include <kos_client.h>

typedef struct shard_t {
	int id;
	hashtable_t *hashtable; 	//The hash table
	pthread_mutex_t locker;
	FILE *file;

	pthread_mutex_t persistence_lock;
	int NPKV;
	unsigned char* bitmap;
} shard_t;

typedef struct shardlist_t {
	shard_t  **list;
	int size;
} shardlist_t;


shardlist_t* shard_init(int size);

shard_t* shard_get(shardlist_t *shardlist, int id);

char* shard_insert_key(shard_t* shard, char key[KV_SIZE], char value[KV_SIZE]);

char* shard_get_key(shard_t* shard, char key[KV_SIZE]);

int shard_total_npkv(shard_t* shard);

#endif
