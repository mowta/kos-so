#ifndef SERVER_T_H
#define SERVER_T_H 1
#include <buffer.h>
#include <shards.h>
#include <semaphore.h>

buffer_t* buffer;
shardlist_t* shardlist;
void *server_thread(void *arg);

char* server_get(shardlist_t* shardlist, int shardid, char key[KV_SIZE]);
char* server_put(shardlist_t* shardlist, int shardid, char key[KV_SIZE], char value[KV_SIZE]);
char* server_remove(shardlist_t* shardlist, int shardid, char key[KV_SIZE]);
KV_t* server_get_all_keys(shardlist_t* shardlist, int shardid, int* dim);

#endif