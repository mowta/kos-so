#ifndef BUFFER_H
#define BUFFER_H 1

#ifndef NUM_CLIENT_THREADS
#define NUM_CLIENT_THREADS 30
#endif

#define GET 0
#define PUT 1
#define REMOVE 2
#define GETALLKEYS 3

#define PROCESSED 1000
#define UNPROCESSED 1001
#define REPLACEABLE	1002 

#include <kos_client.h>
#include <semaphore.h>


typedef struct response_t {
	int command;
	char key[KV_SIZE];
	char value[KV_SIZE];
	void* response; //Como é void*, pode ter vários tipos desde que correctamente inicializado
	int integer; //General porpouse integer
	int shardid;

	pthread_mutex_t lock;
	sem_t valid_response;
	int status;
} response_t;


typedef struct buffer_t {
	response_t **buff; //The buffer response structure

	sem_t posicoesSemInfo; 
	sem_t posicoesComInfo;
	
	int size;

	int proximaEscrita;
	int proximaLeitura;
	pthread_mutex_t lock;
} buffer_t;


buffer_t* buffer_init(int size, int num_servers);

/** Produtores consumidores */
response_t* buffer_pop_request(buffer_t* buffer);
void buffer_insert_request(buffer_t* buffer, response_t* request);
response_t* buffer_new_response(int shardid, int command, char* key, char* value);

#endif